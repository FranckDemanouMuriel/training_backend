package com.example.training_backend.controllers;

import com.example.training_backend.models.Training;
import com.example.training_backend.service.TrainingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@RequestMapping(value = "/api/v1/Trainings")
@RestController
public class TrainingController {

    private TrainingService trainingService;

    @GetMapping("/allTrainings")
    public ResponseEntity<List<Training>> getAllTrainings() {
        return ResponseEntity.ok(trainingService.getAllTraining());
    }

    @GetMapping("/allTrainingsByPeriod")
    public ResponseEntity<List<Training>> getAllTrainingsByPeriod(@RequestParam LocalDate startDate, @RequestParam LocalDate endDate) {
        return ResponseEntity.ok(trainingService.getAllTrainingByPeriod(startDate, endDate));
    }

    @GetMapping("/allTrainingsByPeriod2")
    public ResponseEntity<List<Training>> getAllTrainingsByPeriod2(@RequestParam LocalDate startDate, @RequestParam LocalDate endDate) {
        return ResponseEntity.ok(trainingService.getAllTrainingByPeriod2(startDate, endDate));
    }

    @PostMapping("/")
    public ResponseEntity<Training> createTraining(@RequestBody Training training) {
        return ResponseEntity.ok(trainingService.createNewTraining(training));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Training> updateTraining(@RequestBody Training training, @PathVariable Long id, @RequestParam String docentName,
                                                   @RequestParam LocalDate createDate, @RequestParam String description,
                                                   @RequestParam String name, @RequestParam int price) {
        return ResponseEntity.ok(trainingService.updateTraining(training, id, docentName, createDate, description, name, price));
    }


}
