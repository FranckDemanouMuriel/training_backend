package com.example.training_backend.models;
import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;

import java.time.LocalDate;


@Entity
public class Training {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String description;
  private int price;
  private String docent_name;
  @JsonFormat(pattern="dd-MM-yyyy")
  private LocalDate createDate;


  public Training() {}

  public Training(String name, String description, Integer price, String docent_name, LocalDate createDate) {
    this.name = name;
    this.description = description;
    this.price = price;
    this.docent_name = docent_name;
    this.createDate = createDate;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public void setDocent_name(String docentName) {
    this.docent_name = docentName;
  }

  public void setCreateDate(LocalDate createDate) {
    this.createDate = createDate;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Integer getPrice() {
    return price;
  }

  public String getDocent_name() {
    return docent_name;
  }

  public LocalDate getCreateDate() {
    return createDate;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }
}
