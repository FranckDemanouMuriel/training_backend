package com.example.training_backend.repositories;

import com.example.training_backend.models.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;


@Repository
public interface CustomTrainingRepository extends JpaRepository<Training, Long> {

  @Query("SELECT t FROM Training t WHERE t.createDate >= ?1 and t.createDate <= ?2")
  List<Training> findAllByPeriod(LocalDate startDate, LocalDate endDate);

  List<Training> findAllByCreateDateBetween(LocalDate startDate, LocalDate endDate);


}
