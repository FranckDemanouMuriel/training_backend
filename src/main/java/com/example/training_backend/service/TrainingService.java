package com.example.training_backend.service;

import com.example.training_backend.models.Training;
import com.example.training_backend.repositories.CustomTrainingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.*;

@AllArgsConstructor
@Service
public class TrainingService {
     
    private   CustomTrainingRepository customTrainingRepository;

    public List<Training> getAllTraining() {
        return customTrainingRepository.findAll();
    }

    public List<Training> getAllTrainingByPeriod(LocalDate startDate, LocalDate endDate) {
      return customTrainingRepository.findAllByPeriod(startDate, endDate); 
    }

    public List<Training> getAllTrainingByPeriod2(LocalDate startDate, LocalDate endDate) {
      return customTrainingRepository.findAllByCreateDateBetween(startDate, endDate); 
    }

    public Training createNewTraining(Training training) {
        return customTrainingRepository.save(training);
    }

    public Training updateTraining(Training training, Long id, String docentName,
                                   LocalDate createDate, String description,
                                   String name, int price) {
        training.setId(id);
        training.setDocent_name(docentName);
        training.setCreateDate(createDate);
        training.setDescription(description);
        training.setName(name);
        training.setPrice(price);
        return customTrainingRepository.save(training);
    }

}
